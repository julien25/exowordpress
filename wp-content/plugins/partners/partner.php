<?php
/*
Plugin Name: Partners
Plugin URI: #
Description: Test Plugin
Author: JNguyen
Version: 1
Author URI: #
*/
function wpm_custom_post_type() {

	// On rentre les différentes dénominations de notre custom post type qui seront affichées dans l'administration
	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'partenaire', 'Post Type General Name'),
		// Le nom au singulier
		'singular_name'       => _x( 'partenaire', 'Post Type Singular Name'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Partenaire'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Toutes les partenaires'),
		'view_item'           => __( 'Voir les partenaires'),
		'add_new_item'        => __( 'Ajouter un nouveau partenaire'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer le partenaire'),
		'update_item'         => __( 'Modifier le partenaire'),
		'search_items'        => __( 'Rechercher un partenaire'),
		'not_found'           => __( 'Non trouvée'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	);
	
	// On peut définir ici d'autres options pour notre custom post type
	
	$args = array(
		'label'               => __( 'Partenaire'),
		'description'         => __( ''),
		'labels'              => $labels,
		// On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		/* 
		* Différentes options supplémentaires
		*/
		'show_in_rest' => true,
		'hierarchical'        => false,
		'public'              => true,
		'has_archive'         => true,
		'rewrite'			  => array( 'slug' => 'partenaire'),

	);
	
	// On enregistre notre custom post type qu'on nomme ici "partenaire" et ses arguments
	register_post_type( 'partenaire', $args );

}

add_action( 'init', 'wpm_custom_post_type', 0 );
?>