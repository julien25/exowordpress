<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'exo_wp' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'root' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'PSs:Nqyo_-#^7r5s%F2hd*KXi]1 %-KP={EkZK4_1hz-paY<RE&B[#o$N_[Kk39U' );
define( 'SECURE_AUTH_KEY',  '6R9`%3E$is}>1{0%/D<U-JS|9n9568bQz-lX~7<^.M0;/1n@qA<Q Yv/Ruv7?*8i' );
define( 'LOGGED_IN_KEY',    '&ud.{oO5=wP8z+BH7v)4x4`uE(yVnQP Uhny kgga`BP)Qf-T<#3R8Zd[!)(39[j' );
define( 'NONCE_KEY',        ']mzYx76%t,xIMRaa&zp.{sl;.Pi={ge?}um1VY?g,vM?0>b<W5XN6seK(W8rmG$x' );
define( 'AUTH_SALT',        ')2rY`_vS*v3$(Sz&$B!GvAmt2Jj<sny=t^_>C(4Q4IxukQN:@,-IwQ10>;87@-L|' );
define( 'SECURE_AUTH_SALT', 'TPI%;PP+rX~p8p>v2L9v>mfyopFwOTi>@7wO4a2D9t%-OgW_8E-7#4z!*`|xB]3Z' );
define( 'LOGGED_IN_SALT',   'rn H5$N7U~eRpX%AT7X0}i{$+l1G{YVk TElB-AP$tL{mG}4^`i#t%5uNfMu&,9i' );
define( 'NONCE_SALT',       'OnO-G$A[E(UhcvB0t1#tIXRZ^R^!#b|XbYJ.ULaiFn{ufBw1nH?9u;H=#T-XXd<;' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
